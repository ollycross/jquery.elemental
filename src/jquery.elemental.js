import $ from 'jquery';

class Elemental {
  constructor(string = 'div', attr = {}, data = {}) {
    this.patterns = {
      tag: /^\w+/,
      class: /\.[\w-]+/g,
      id: /#([\w-]+)/,
    };

    const {
      tagName,
      classes,
      id,
    } = this.processString(string);

    if (tagName) {
      const $element = $(document.createElement(tagName));
      if (classes) {
        Object.assign(attr, {
          class: classes.map(item => item.substr(1)).join(' '),
        });
      }
      if (id) {
        Object.assign(attr, {
          id: id[0].substr(1),
        });
      }
      $element.attr(attr).data(data);

      return $element;
    }

    throw new Error(`Could not match tagname in "${string}"`);
  }

  processString(string) {
    return {
      tagName: string.match(this.patterns.tag),
      classes: string.match(this.patterns.class),
      id: string.match(this.patterns.id),
    };
  }
}

const factory = (string, attr, data) => new Elemental(string, attr, data);

export {
  Elemental,
};

export default factory;
