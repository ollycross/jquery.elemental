const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = (env, argv) => ({
  entry: {
    main: './src/jquery.elemental.js',
  },
  devtool: argv.mode === 'development' ? 'source-map' : false,
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: 'jquery.elemental.js',
    libraryTarget: 'umd',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
      },
    }],
  },
  plugins: [
    new WebpackNotifierPlugin({
      alwaysNotify: true,
    }),
  ],
});
